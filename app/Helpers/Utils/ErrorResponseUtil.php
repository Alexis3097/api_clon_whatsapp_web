<?php

namespace App\Helpers\Utils;

  class ErrorResponseUtil
{
    public static function ResponseErrorValidate($errors): array
    {
        return [
            'success' => false,
            'message' => 'Unprocessable Content',
            'status' => 422,
            'data' => $errors
        ];
  }
}
