<?php

namespace App\Services;
use App\Helpers\Utils\LogMessage;
use App\Helpers\Utils\ResponseUtil;
use App\IRepositories\IChatRepository;
use App\IServices\IChatService;
use Exception;
class ChatService implements IChatService
{
    private IChatRepository $_IChatRepository;

    /**
     * @param IChatRepository $IChatRepository
     */
    function __construct(IChatRepository $IChatRepository)
    {

        $this->_IChatRepository = $IChatRepository;
    }

    /**
     * @inheritDoc
     */
    public function index(): array
    {
        try {
            return $this->_IChatRepository->index();
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    public function create($data)
    {
        try {
            return $this->_IChatRepository->create($data);
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }
}
