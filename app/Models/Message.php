<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Message extends Model
{
    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'state_id',
        'user_id',
        'chat_id',
        'is_forwarded',
        'content',
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function state(){
        return $this->belongsTo(State::class);
    }
    public function chat(){
        return $this->belongsTo(Chat::class);
    }

    /**
     * Obtiene la hora en formato 12 hrs
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
        protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon::parse($value)->format('h:i A')
        );
    }
}
