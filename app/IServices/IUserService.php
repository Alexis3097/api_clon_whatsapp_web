<?php

namespace App\IServices;
use App\Models\User;
interface IUserService
{
    /**
     * Obtiene todos los usuarios
     * @return array
     */
    public function getAll(): array;

    /**
     * @param $data
     * @return array
     */
    public function create($data) :array;

    /**
     * Obtiene el usuario por id especificado
     * @param $id
     * @return array
     */
    public function getById($id): array;

    /**
     * Actualiza el usuario con los nuevos datos y el id recividos
     * @param $data
     * @param $id
     * @return array
     */
    public function update($data, $id) : array;

    /**
     * Elimina el usuario con el id especificado
     * @param $id
     * @return array
     */
    public function delete($id): array;
}
