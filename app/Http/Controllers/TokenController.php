<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class TokenController extends Controller
{
    /**
     * @OA\Post(
     * path="/api/v1/sanctum/token",
     * summary="login",
     * description="Inicio de sesion con correo, contraseña y nombre del dispositivo",
     * operationId="authLogin",
     * tags={"auth"},

     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(ref="#/components/schemas/Authenticate"),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Error con las credenciales",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Credenciales incorrectas")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Autenticado correctamente")
     *        )
     *     )
     * )
     */
  public function __invoke(Request $request)
  {
    $request->validate([
      'cell_phone' => 'required',
      'password' => 'required',
      'device_name' => 'required',
    ]);

    $user = User::where('cell_phone', $request->cell_phone)->first();

    if (!$user || !Hash::check($request->password, $user->password)) {
      throw ValidationException::withMessages([
        'password' => ['Las credenciales son incorrectas.'],
      ]);
    }

    $token = $user->createToken($request->device_name)->plainTextToken;

    return response()->json(['token' => $token], 200);
  }
}
