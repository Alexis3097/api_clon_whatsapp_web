<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Helpers\Utils\ResponseUtil;
use Response;

/**
 * @OA\Info(title="API",
 *     version="1.0",
 *     description="L5 Swagger OpenApi description",
 *     @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 * @OA\PathItem(
 *     path="v1/",
 *     @OA\Parameter(
 *         name="version",
 *         in="path",
 *         required=true,
 *         description="API version",
 *         @OA\Schema(type="string")
 *     )
 * )
 * @OA\Server(url="http://localhost")
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }
}
