<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TokenController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\MessageController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post(ENV('API_VERSION').'/sanctum/token', TokenController::class);
Route::middleware(['auth:sanctum'])->prefix(ENV('API_VERSION'))->group(function () {
    Route::get('users/auth',[UserController::class,'auth']);

    Route::get('chats',[ChatController::class,'index']);
    Route::post('chats',[ChatController::class,'store']);
    Route::get('chats/{chatId}/messages',[MessageController::class,'GetMessagesXChat']);
    Route::post('chats/{chatId}/messages',[MessageController::class,'store']);
    Route::post('message/{chatId}',[MessageController::class,'checked']);
    Route::get('contacts',[UserController::class,'index']);

});



