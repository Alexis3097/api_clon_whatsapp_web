<?php

namespace App\Schema\Chat;
use App\Helpers\Utils\ErrorResponseUtil;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * @OA\Schema(
 *     schema="StoreChat",
 *     required = {"recipient_id", "sender_id"}
 * )
 */
class StoreChat extends  FormRequest
{
    /** @OA\Property(property="recipient_id", type="integer") */
    public string $recipient_id;

    /** @OA\Property(property="sender_id", type="integer") */
    public string $sender_id;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'recipient_id' => 'required',
            'sender_id' => 'required|different:recipient_id',
        ];
    }

    public function messages(): array
    {
        return [
            'recipient_id.required' => 'El :attribute es requerido',
            'sender_id.required' => 'El :attribute es requerido',
            'sender_id.different' => 'Los id deben ser diferentes',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $MessagesError = ErrorResponseUtil::ResponseErrorValidate($validator->errors());
        throw new HttpResponseException(response()->json($MessagesError, 422));
    }
}
