<?php

namespace App\Repositories;
use App\Helpers\Utils\LogMessage;
use App\Helpers\Utils\ResponseUtil;
use App\IRepositories\IChatRepository;
use App\Models\Chat;
use App\Models\Message;
use App\Models\User;
use App\Models\ChatParticipant;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class ChatRepository implements IChatRepository
{

    /**
     * @inheritDoc
     */
    public function index(): array
    {
        try{
            $user = Auth::user();
            $userChatsWithMessages = ChatParticipant::where('user_id', $user->id)
                ->join('chats', 'chat_participants.chat_id', '=', 'chats.id')
                ->select('chat_participants.*')
                ->distinct()
                ->get()
                ->filter(function ($chat) {
                    $messages = Message::where('chat_id', $chat->chat_id)->exists();
                    return $messages;
                })
                ->each(function ($chat) use ($user){
                    $messages = Message::where('chat_id', $chat->chat_id)->get();
                    $chat->messages = $messages;
                    $unreadMessages = $messages->where('state_id', 3)->where('user_id','!=', $user->id)->count();
                    $chat->NoMensjaesNoLeidos = $unreadMessages;
                })
                ->sortByDesc(function ($chat) {
                    return $chat->messages->max('created_at');
                });

            $userChatsWithMessages->each(function ($chat) use ($user){
                $otherParticipantId = ChatParticipant::where('chat_id', $chat->chat_id)
                    ->where('user_id', '!=', $user->id)
                    ->value('user_id');

                $otherParticipant = User::find($otherParticipantId);

                $chat->name = $otherParticipant->name;
                $chat->profile_picture = $otherParticipant->profile_picture;
                $chat->recipient_id = $otherParticipant->id;
            });



            return ResponseUtil::makeResponse("OK", $userChatsWithMessages->values());
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    public function create($data): array
    {
        try {
            $sender_id = $data["sender_id"];
            $recipient_id = $data["recipient_id"];

            // Verificamos si estos dos contactos ya tienen un chat
            $chatId = ChatParticipant::whereIn('user_id', [$sender_id, $recipient_id])
                ->select('chat_id')
                ->groupBy('chat_id')
                ->havingRaw('COUNT(DISTINCT user_id) = 2')
                ->first();

            if ($chatId == null) {
                // Si no tienen, creamos uno
                DB::transaction(function () use ($sender_id, $recipient_id, &$chatId) {
                    $chat = Chat::create([]);

                    ChatParticipant::create([
                        'chat_id' => $chat->id,
                        'user_id' => $sender_id,
                    ]);

                    ChatParticipant::create([
                        'chat_id' => $chat->id,
                        'user_id' => $recipient_id,
                    ]);

                    $chatId = [
                        'chat_id'=>$chat->id
                    ];
                });
            }

            return ResponseUtil::makeResponse("OK", $chatId);
        } catch (Exception $e) {
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }


}
