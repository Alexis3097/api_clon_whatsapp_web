<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>


## Configuración inicial

Antes de ejecutar la aplicación Laravel, asegúrate de seguir estos pasos de configuración inicial:

1. Clona el repositorio o descarga el código fuente de la aplicación Laravel.

2. Crea el archivo `.env` en la raíz del proyecto. Puedes copiar el contenido del archivo `.env.example` y ajustarlo según tu entorno.

3. Genera una nueva clave de aplicación utilizando el siguiente comando de Artisan:

   ```bash
   php artisan key:generate
   ```

   Esto generará una clave única para tu aplicación Laravel.

## Instalación de dependencias

1. Asegúrate de tener Composer instalado en tu sistema. Puedes descargarlo desde https://getcomposer.org si aún no lo tienes.

2. Ejecuta el siguiente comando para instalar las dependencias del proyecto:

   ```bash
   composer install
   ```

   Esto descargará todas las dependencias definidas en el archivo `composer.json` y las instalará en tu proyecto.

## Migraciones de la base de datos

1. Configura tu conexión de base de datos en el archivo `.env` según tus ajustes locales.

2. Ejecuta las migraciones para crear las tablas de la base de datos:

   ```bash
   php artisan migrate
   ```

   Esto creará las tablas definidas en las migraciones en tu base de datos.

## Ejecución de seeders

Si deseas poblar la base de datos con datos de prueba, puedes ejecutar los seeders correspondientes:

```bash
php artisan db:seed
```

Esto ejecutará los seeders definidos en la aplicación y llenará la base de datos con datos de ejemplo.

¡Listo! Ahora estás listo para ejecutar y utilizar la aplicación Laravel. Puedes iniciar el servidor de desarrollo con el comando `php artisan serve` y acceder a la aplicación a través de la URL proporcionada. Asegúrate de consultar la documentación adicional y los comandos disponibles en la documentación oficial de Laravel.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
