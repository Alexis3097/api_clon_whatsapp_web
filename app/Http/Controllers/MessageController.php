<?php

namespace App\Http\Controllers;
use App\Schema\Message\StoreMessage;
use Exception;
use App\Helpers\Enums\ResponseMessages;
use App\IServices\IMessageService;
use Illuminate\Http\Request;
use App\Schema\Message\StoreMessage as StoreMessageRequest;
class MessageController extends Controller
{
    protected IMessageService $_IMessageService;

    function __construct(IMessageService $IMessageService)
    {
        $this->middleware('auth:sanctum');
        $this->_IMessageService = $IMessageService;
    }


    public function GetMessagesXChat($chayId){
        try {
            $chats = $this->_IMessageService->GetMessagesXChat($chayId);
            if ($chats["success"]) {
                return $this->sendResponse($chats["data"], ResponseMessages::GET_RESOURCES());
            } else {
                return $this->sendError($chats["message"]);
            }
        } catch (Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }
    /**
     * @OA\Post(
     * path="/api/v1/chats/{chatId}/messages",
     * summary="save messages",
     * description="Guarda un mensaje",
     * operationId="saveMessage",
     * tags={"message"},
     * security={ {"sanctum": {} }},
     *      @OA\Parameter(
     *         name="chatId",
     *         in="path",
     *         description="chatId",
     *         required=true,
     *      ),
     * @OA\RequestBody(
     *    required=true,
     *    description="datos para guardar un mensaje",
     *    @OA\JsonContent(ref="#/components/schemas/StoreMessage"),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Error de validacion",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Error de validacion")
     *        )
     *     ),
     * @OA\Response(
     *    response=200,
     *    description="OK",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string")
     *        )
     *     )
     * )
     */
    public  function store(StoreMessageRequest $request, $chat_id){
        try {

            $chats = $this->_IMessageService->store($chat_id, $request->all());
            if ($chats["success"]) {
                return $this->sendResponse($chats["data"], ResponseMessages::GET_RESOURCES());
            } else {
                return $this->sendError($chats["message"]);
            }
        } catch (Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }
    public  function checked($chatId){
        try {

            $chats = $this->_IMessageService->checked($chatId);
            if ($chats["success"]) {
                return $this->sendResponse($chats["data"], ResponseMessages::GET_RESOURCES());
            } else {
                return $this->sendError($chats["message"]);
            }
        } catch (Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }
}
