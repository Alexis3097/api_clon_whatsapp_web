<?php

namespace Database\Seeders;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(1)->create(
            [
                'cell_phone' => '9611234567',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
                'profile_picture'=>'https://cdn.quasar.dev/img/avatar' . rand(1, 6) .'.jpg',
                'name'=>'Admin'
            ]
        );
        \App\Models\User::factory(1)->create(
            [
                'cell_phone' => '12345',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
                'profile_picture'=>'https://cdn.quasar.dev/img/avatar' . rand(1, 6) .'.jpg',
                'name'=>'luis'
            ]
        );
    }
}
