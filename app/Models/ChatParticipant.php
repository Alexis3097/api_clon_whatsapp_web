<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class ChatParticipant extends Model
{
    use HasFactory;
    protected $table = 'chat_participants';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'chat_id',
        'user_id',
    ];

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function messages()
    {
        return $this->hasMany(Message::class, 'chat_id');
    }

}
