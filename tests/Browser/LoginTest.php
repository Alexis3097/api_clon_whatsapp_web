<?php

namespace Tests\Browser;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    use RefreshDatabase;
    public function testUserCanLogin()
    {

      $user = User::factory()->make();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit(':5173/login')
                ->waitFor('@login')
                ->type('email',   $user->name)
                ->type('password', 'password')
                ->press('Login')
                ->assertPathIs('/home')
                ->assertSee($user->name);

        });
    }
}
