<?php

namespace App\Services;
use App\Helpers\Utils\LogMessage;
use App\Helpers\Utils\ResponseUtil;
use App\IRepositories\IMessageRepository;
use App\IServices\IMessageService;
use Exception;
class MessageService implements IMessageService
{
    protected IMessageRepository $_IMessageRepository;

    function __construct(IMessageRepository $IMessageRepository)
    {
        $this->_IMessageRepository = $IMessageRepository;
    }

    public function GetMessagesXChat($chayId): array
    {
        try {
            return $this->_IMessageRepository->GetMessagesXChat($chayId);
        } catch (Exception $e) {
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    public function store($chat_id,$data): array
    {
        try {
            return $this->_IMessageRepository->store($chat_id,$data);
        } catch (Exception $e) {
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    public function checked($chatId)
    {
        try {
            return $this->_IMessageRepository->checked($chatId);
        } catch (Exception $e) {
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }
}
