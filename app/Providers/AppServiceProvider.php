<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*
        * ==================================================================
        *                    services
        * ==================================================================
        */
        $this->app->bind(
            'App\IServices\IUserService',
            'App\Services\UserService'
        );
        $this->app->bind(
            'App\IServices\IChatService',
            'App\Services\ChatService'
        );

        $this->app->bind(
            'App\IServices\IMessageService',
            'App\Services\MessageService'
        );
        /*
         * ==================================================================
         *                    repositories
         * ==================================================================
         */

        //repositories
        $this->app->bind(
            'App\IRepositories\IUserRepository',
            'App\Repositories\UserRepository'
        );
        $this->app->bind(
            'App\IRepositories\IChatRepository',
            'App\Repositories\ChatRepository'
        );
        $this->app->bind(
            'App\IRepositories\IMessageRepository',
            'App\Repositories\MessageRepository'
        );



        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
