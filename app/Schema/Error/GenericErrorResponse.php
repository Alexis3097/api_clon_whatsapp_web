<?php

namespace App\Schema\Error;
/**
 * @OA\Schema(
 *     schema="GenericErrorResponse",
 * )
 */
class GenericErrorResponse
{
    /** @OA\Property(property="success", type="string", example="false") */
    public string $success;

    /** @OA\Property(property="message", type="string", example="Message description") */
    public string $message;
}
