<?php

namespace App\Schema\User;

use App\Helpers\Utils\ErrorResponseUtil;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * @OA\Schema(
 *     schema="StoreUser",
 *     required = {"name", "email", "password"}
 * )
 */
class StoreUser extends FormRequest
{
    /** @OA\Property(property="name", type="string",  example="Alexis") */
    public string $name;

    /** @OA\Property(property="email", type="string", format="email") */
    public string $email;

    /** @OA\Property(property="password", type="string") */
    public string $password;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'required'
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'El :attribute es requerido',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $MessagesError = ErrorResponseUtil::ResponseErrorValidate($validator->errors());
        throw new HttpResponseException(response()->json($MessagesError, 422));
    }
}
