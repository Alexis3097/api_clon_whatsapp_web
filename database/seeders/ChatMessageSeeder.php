<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Chat;
use App\Models\ChatParticipant;
use App\Models\Message;
class ChatMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $chat_1 = Chat::create([]);
        $chat_2 = Chat::create([]);
        $chat_3 = Chat::create([]);

        ChatParticipant::create([
            'chat_id'=>$chat_1->id,
            'user_id'=>11,
        ]);
        ChatParticipant::create([
            'chat_id'=>$chat_1->id,
            'user_id'=>1,
        ]);

        ChatParticipant::create([
            'chat_id'=>$chat_2->id,
            'user_id'=>11,
        ]);
        ChatParticipant::create([
            'chat_id'=>$chat_2->id,
            'user_id'=>2,
        ]);

        ChatParticipant::create([
            'chat_id'=>$chat_3->id,
            'user_id'=>11,
        ]);
        ChatParticipant::create([
            'chat_id'=>$chat_3->id,
            'user_id'=>3,
        ]);


        Message::create([
            'state_id'=>3,
            'user_id'=>11,
            'chat_id'=>1,
            'is_forwarded'=>0,
            'content'=>'Holaaa',
        ]);
        Message::create([
            'state_id'=>3,
            'user_id'=>2,
            'chat_id'=>2,
            'is_forwarded'=>0,
            'content'=>'¿Como has estado?',
        ]);
        Message::create([
            'state_id'=>3,
            'user_id'=>3,
            'chat_id'=>3,
            'is_forwarded'=>0,
            'content'=>'Holaa',
        ]);
        Message::create([
            'state_id'=>3,
            'user_id'=>3,
            'chat_id'=>3,
            'is_forwarded'=>0,
            'content'=>'oye',
        ]);
        Message::create([
            'state_id'=>3,
            'user_id'=>3,
            'chat_id'=>3,
            'is_forwarded'=>0,
            'content'=>'oye',
        ]);
    }
}
