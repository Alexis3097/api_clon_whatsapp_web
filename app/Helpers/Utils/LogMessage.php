<?php

namespace App\Helpers\Utils;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
 class LogMessage
{
    public static function Log($message){
        Log::info($message .' '. Carbon::now());
    }
}
