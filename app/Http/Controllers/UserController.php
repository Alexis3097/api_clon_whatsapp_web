<?php

namespace App\Http\Controllers;

use App\Helpers\Enums\ResponseMessages;
use App\IServices\IUserService;
use App\Models\User;
use App\Schema\User\StoreUser as StoreUserRequest;
use App\Schema\User\UpdateUser as UpdateUserRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations as OA;
class UserController extends Controller
{

    protected IUserService $_IUserService;
    function __construct(IUserService $IUserService)
    {
        $this->middleware('auth:sanctum');
        $this->_IUserService = $IUserService;

    }


    /**
     * * @OA\Examples(
     *     example="ResponseAllUsers",
     *     summary="Response with all users",
     *     value={
     *          "success": true,
     *          "data": {{
     *              "name": "string",
     *              "email": "email@example.com",
     *              "updated_at": "2023-01-20T17:30:03.000000Z",
     *              "created_at": "2023-01-20T17:30:03.000000Z",
     *              "id": "integer"
     *          }},
     *     "message": "Registro guardado"
     *     }
     * )
     * @OA\Get(
     *     path="/api/v1/contacts",
     *     summary="Show all users",
     *     description="get a list of users",
     *     operationId="getUsers",
     *     security={ {"sanctum": {} }},
     *     tags={"contact"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             examples={
     *                 "example": @OA\Schema(ref="#/components/examples/ResponseAllUsers", example="ResponseAllUsers"),
     *             },
     *          )
     *     ),
     *      @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *        @OA\JsonContent(ref="#/components/schemas/ValidateErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Bad Request",
     *         @OA\JsonContent(ref="#/components/schemas/GenericErrorResponse")
     *     )
     * )
     */

    public function index()
    {
        try {
            $users = $this->_IUserService->getAll();
            if ($users["success"]) {
                return $this->sendResponse($users["data"], ResponseMessages::GET_RESOURCES());
            } else {
                return $this->sendError($users["message"]);
            }
        } catch (Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }







    /**
     * @OA\Examples(
     *     example="User",
     *     summary="Get user",
     *     value={
     *          "success": true,
     *          "data": {
     *              "name": "string",
     *              "email": "email@example.com",
     *              "updated_at": "2023-01-20T17:30:03.000000Z",
     *              "created_at": "2023-01-20T17:30:03.000000Z",
     *              "id": "integer"
     *          },
     *     "message": "Registro guardado"
     *     }
     * )
     * @OA\Get(
     *     path="/api/v1/contacts/auth",
     *     summary="Get uset authenticated",
     *     description="Get uset authenticated",
     *     operationId="GetAuth",
     *     security={ {"sanctum": {} }},
     *     tags={"contact"},
     *     @OA\Response(
     *         response=200,
     *         description="Get user with specific user.",
     *         @OA\JsonContent(
     *       examples={
     *                 "example": @OA\Schema(ref="#/components/examples/User", example="User"),
     *             },
     *        )
     *     ),
     *      @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *        @OA\JsonContent(ref="#/components/schemas/ValidateErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Bad Request",
     *         @OA\JsonContent(ref="#/components/schemas/GenericErrorResponse")
     *     )
     * )
     */
    public function auth(){
        try{
            return $this->sendResponse(Auth::user(), ResponseMessages::GET_RESOURCES());
        }catch (Exception $e){
            return $this->sendError($e->getMessage(), 500);
        }
    }
}
