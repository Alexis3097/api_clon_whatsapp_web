<?php

namespace App\Schema\Message;
use App\Helpers\Utils\ErrorResponseUtil;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * @OA\Schema(
 *     schema="StoreMessage",
 *     required = {"message","sender_id"}
 * )
 */
class StoreMessage extends FormRequest
{
    /** @OA\Property(property="message", type="string") */
    public string $message;

    /** @OA\Property(property="sender_id", type="string") */
    public string $sender_id;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'message' => 'required',
            'sender_id' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'message.required' => 'El :attribute es requerido',
            'sender_id.required' => 'El :attribute es requerido',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $MessagesError = ErrorResponseUtil::ResponseErrorValidate($validator->errors());
        throw new HttpResponseException(response()->json($MessagesError, 422));
    }
}
