<?php

namespace App\IServices;

interface IChatService
{
    /**
     * Obtiene chats recientes y paginados, ordenados por el último chat que recibió un mensaje.
     * @return array
     */
    public function index() : array;

    public function create($data);
}
