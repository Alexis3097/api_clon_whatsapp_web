<?php

namespace App\Http\Controllers;

use App\Helpers\Enums\ResponseMessages;
use App\IServices\IChatService;
use App\Schema\Chat\StoreChat as StoreChatRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;
use Exception;
class ChatController extends Controller
{
    protected IChatService $_IChatService;

    function __construct(IChatService $IChatService)
    {
        $this->middleware('auth:sanctum');
        $this->_IChatService = $IChatService;
    }

    /**
     * * @OA\Examples(
     *     example="ResponseChats",
     *     summary="Response with paginate chats",
     *     value={
     *          "success": true,
     *          "data": {{
     *              "name": "string",
     *              "email": "email@example.com",
     *              "updated_at": "2023-01-20T17:30:03.000000Z",
     *              "created_at": "2023-01-20T17:30:03.000000Z",
     *              "id": "integer"
     *          }},
     *     "message": "Registros obtenidos"
     *     }
     * )
     * @OA\Get(
     *     path="/api/v1/chats",
     *     summary="Show paginate chats",
     *     description="get a list of chats",
     *     operationId="getChats",
     *     security={ {"sanctum": {} }},
     *     tags={"chat"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             examples={
     *                 "example": @OA\Schema(ref="#/components/examples/ResponseChats", example="ResponseChats"),
     *             },
     *          )
     *     ),
     *      @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *        @OA\JsonContent(ref="#/components/schemas/ValidateErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Bad Request",
     *         @OA\JsonContent(ref="#/components/schemas/GenericErrorResponse")
     *     )
     * )
     */

    public function index(){
        try {
            $chats = $this->_IChatService->index();
            if ($chats["success"]) {
                return $this->sendResponse($chats["data"], ResponseMessages::GET_RESOURCES());
            } else {
                return $this->sendError($chats["message"]);
            }
        } catch (Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }

    public function store(StoreChatRequest $request): JsonResponse
    {
        try {
            $chat = $this->_IChatService->create($request->all());
            if ($chat["success"]) {
                return $this->sendResponse($chat["data"], ResponseMessages::POST_SUCCESS());
            } else {
                return $this->sendError($chat["message"]);
            }
        } catch (Exception $e) {
            return $this->sendError($e->getMessage(), 500);
        }
    }


}
