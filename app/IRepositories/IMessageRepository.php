<?php

namespace App\IRepositories;

interface IMessageRepository
{
    public function GetMessagesXChat($chayId);

    public function store($chat_id, $data);
    public function checked($chatId);
}
