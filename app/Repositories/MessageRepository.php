<?php

namespace App\Repositories;
use App\Helpers\Utils\LogMessage;
use App\Helpers\Utils\ResponseUtil;
use App\IRepositories\IMessageRepository;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;
class MessageRepository implements IMessageRepository
{
    public function GetMessagesXChat($chayId)
    {
        try {

            $messages = Message::where('chat_id', $chayId)->get();
            return ResponseUtil::makeResponse("OK", $messages);
        } catch (Exception $e) {
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    public function store($chat_id, $data)
    {
        $message = [];
        try {
            DB::transaction(function () use ($data, $chat_id, &$message) {
                $message = Message::create([
                    'state_id' => 3,
                    'user_id' => $data["sender_id"],
                    'chat_id' => $chat_id,
                    'is_forwarded' => 0,
                    'content' => $data["message"],
                ]);


            });
            return ResponseUtil::makeResponse("OK", $message);
        } catch (Exception $e) {
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    public function checked($chatId)
    {
        try {
            $user = Auth::user();
            $messages = Message::where('chat_id', $chatId)
                ->where('user_id', '!=', $user->id)
                ->update(['state_id' => 4]);
            return ResponseUtil::makeResponse("OK", $messages);
        } catch (Exception $e) {
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }
}
