<?php

namespace App\Schema\Auth;

/**
 * @OA\Schema(
 *     schema="Authenticate",
 *      required={
 *     "cell_phone",
 *      "password",
 *      "device_name",
 *     }
 * )
 */
class Authenticate
{
    /** @OA\Property(property="cell_phone", type="string", example="9611234567") */
    public string $cell_phone;

    /** @OA\Property(property="password", type="string", format="password", example="password") */
    public string $password;

    /** @OA\Property(property="device_name", type="string",  example="insomnia") */
    public string $device_name;
}
