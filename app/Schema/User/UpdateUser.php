<?php

namespace App\Schema\User;
use App\Helpers\Utils\ErrorResponseUtil;
use Illuminate\Contracts\Validation\Validator;
use \Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
/**
 * @OA\Schema(
 *     schema="UpdateUser",
 * )
 */
class UpdateUser extends FormRequest
{
    /** @OA\Property(property="name", type="string") */
    public string $name;

    /** @OA\Property(property="email", type="string", format="email") */
    public string $email;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'string',
            'email' => 'string',
        ];
    }

    public function messages(): array
    {
        return [
            'name.string' => 'El :attribute debe ser string',
            'email.string' => 'El :attribute debe ser string',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $MessagesError = ErrorResponseUtil::ResponseErrorValidate($validator->errors());
        throw new HttpResponseException(response()->json($MessagesError), 422);
    }
}
