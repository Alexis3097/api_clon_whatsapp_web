<?php

namespace App\IServices;

interface IMessageService
{
    public function GetMessagesXChat($chayId);
    public function store($chat_id,$data);
    public function checked($chatId);
}
