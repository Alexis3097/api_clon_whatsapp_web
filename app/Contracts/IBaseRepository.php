<?php

namespace App\Contracts;

interface IBaseRepository
{
    /**
     * Obtiene una coleccion de un modelo especidicado
     * @return array
     */
    public function getAll(): array;

    /**
     * Crea un registro del modelo especificadp
     * @param $data
     * @return array
     */
    public function create($data): array;

    /**
     * Obtiene un registro con el id especificado
     * @param $id
     * @return array
     */
    public function getById($id): array;

    /**
     * Actualiza un registro con los datos proporcionados del id especificado
     * @param $data
     * @param $id
     * @return array
     */
    public function update($data, $id): array;

    /**
     * Elimina un registro con el id proporcionado
     * @param $id
     * @return array
     */
    public function delete($id): array;
}
