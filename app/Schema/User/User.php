<?php

namespace App\Schema\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Helpers\Utils\ErrorResponseUtil;
/**
 * @OA\Schema(
 *     schema="User",
 * )
 */
class User extends  FormRequest
{
    /** @OA\Property(property="name", type="string",  example="Alexis") */
    public string $name;

    /** @OA\Property(property="email", type="string", format="email") */
    public string $email;

    /** @OA\Property(property="email_verified_at", type="string") */
    public string $email_verified_at;

    /** @OA\Property(property="two_factor_secret", type="string") */
    public string $two_factor_secret;

    /** @OA\Property(property="two_factor_recovery_codes", type="string") */
    public string $two_factor_recovery_codes;

    /** @OA\Property(property="two_factor_confirmed_at", type="string") */
    public string $two_factor_confirmed_at;

    /** @OA\Property(property="created_at", type="string") */
    public string $created_at;

    /** @OA\Property(property="updated_at", type="string") */
    public string $updated_at;

    public function authorize(): bool
    {
        return true;
    }
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'email' => 'required|string'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $MessagesError = ErrorResponseUtil::ResponseErrorValidate($validator->errors());
        throw new HttpResponseException(response()->json($MessagesError), 422);
    }
}
