<?php

namespace App\Repositories;

use App\IRepositories\IUserRepository;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Exception;
use App\Helpers\Utils\LogMessage;
use App\Helpers\Utils\ResponseUtil;
class UserRepository implements IUserRepository
{
    /**
     * @inherit
     */
    public function getAll(): array
    {
        try{
            $users = User::orderBy('name', 'asc')->paginate(10);
            return ResponseUtil::makeResponse("OK", $users);
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    /**
     * @inherit
     */
    public function create($data) : array
    {
        try {
            DB::beginTransaction();
            $user = User::create($data);
            DB::commit();
            return ResponseUtil::makeResponse("OK", $user);
        } catch (Exception $e) {
            DB::rollback();
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    /**
     * @inherit
     */
    public function getById($id): array
    {
        try{
            return ResponseUtil::makeResponse("OK", User::find($id));
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    /**
     * @inherit
     */
    public function update($data, $id) : array
    {
       try{
           $user = User::findOrFail($id);
           $user->fill($data);
           $user->save();
           return ResponseUtil::makeResponse("OK", $user);
       }catch (Exception $e){
           LogMessage::Log($e->getMessage());
           return ResponseUtil::makeError($e->getMessage());
       }
    }

    /**
     * @inherit
     */
    public function delete($id) : array
    {
        try{
            $user = User::findOrFail($id);
            $user->delete();
            return ResponseUtil::makeResponse("OK", $user);
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }
}
