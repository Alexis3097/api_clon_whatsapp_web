<?php

namespace App\Services;
use App\Helpers\Utils\ResponseUtil;
use App\IRepositories\IUserRepository;
use App\IServices\IUserService;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Utils\LogMessage;
class UserService implements IUserService
{

    private IUserRepository $_IUserRepository;

    /**
     * @param IUserRepository $IUserRepository
     */
    function __construct(IUserRepository $IUserRepository){

        $this->_IUserRepository = $IUserRepository;
    }
    /**
     *  @inherit
     */
    public function create($data) : array
    {
        try{
            $data["password"] = Hash::make($data["password"]);
            return $this->_IUserRepository->create($data);
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    /**
     *  @inherit
     */
    public function getAll(): array
    {
        try {
            return $this->_IUserRepository->getAll();
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    /**
     * @inherit
     */
    public function getById($id): array
    {
        try {
            return $this->_IUserRepository->getById($id);
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function update($data, $id): array
    {
        try {
            return $this->_IUserRepository->update($data, $id);
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function delete($id) : array
    {
        try {
            return $this->_IUserRepository->delete($id);
        }catch (Exception $e){
            LogMessage::Log($e->getMessage());
            return ResponseUtil::makeError($e->getMessage());
        }
    }
}
