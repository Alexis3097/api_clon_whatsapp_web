<?php

namespace App\Schema\Error;
/**
 * @OA\Schema(
 *     schema="ValidateErrorResponse",
 * )
 */
class ValidateErrorResponse
{
    /** @OA\Property(property="success", type="string", example="false") */
    public string $success;

    /** @OA\Property(property="message", type="string", example="Unprocessable Entity") */
    public string $message;

    /** @OA\Property(property="status", type="string", example="422") */
    public string $status;

    /** @OA\Property(property="data", type="string", example="{errors}") */
    public string $data;
}
