<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use App\Models\User;
class UserTest extends TestCase
{
     use RefreshDatabase;
    /** @test
     * @author Cristian Alexis Montoya Arguello
     */
    public function get_user_list()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->get('/api/users');

        $response->assertOk();
    }

    /** @test
     * @author Cristian Alexis Montoya Arguello
     */
    public function save_user(){
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
        $response = $this->post('api/users', [
            'name' => 'Test User',
            'email' => 'test@example.com',
            'password' => 'password',
        ]);

        $response->assertOk();
    }

    /** @test
     * @author Cristian Alexis Montoya Arguello
     */
    public function get_user_by_id(){
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->get('api/users/1');
        $response->assertOk();
    }
}
