<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\State;
use Carbon\Carbon;
class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            ['description' => 'Enviando',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                ],
            ['description' => 'Recibido en servidor',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                ],
            ['description' => 'Usuario recibió mensaje',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                ],
            ['description' => 'Leído',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                ],
        ];
        State::insert($states);
    }
}
