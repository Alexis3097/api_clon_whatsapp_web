<?php

namespace App\Helpers\Enums;
use MyCLabs\Enum\Enum;

/**
 * @method static POST_SUCCESS()
 * @method static GET_RESOURCES()
 * @method static UPDATE_SUCCESS()
 * @method static DELETE_SUCCESS()
 */
final class ResponseMessages extends  Enum
{
    private const GET_RESOURCES = 'Registros encontrados';
    private const UPDATE_SUCCESS = 'Registro actualizado';
    private const POST_SUCCESS = 'Registro guardado';
    private const DELETE_SUCCESS = 'Registro eliminado';
}
