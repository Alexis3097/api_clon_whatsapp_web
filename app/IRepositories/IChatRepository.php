<?php

namespace App\IRepositories;

interface IChatRepository
{

    /**
     * Obtiene chats recientes y paginados, ordenados por el último chat que recibió un mensaje.
     * @return array
     */
    public function index();

    public  function create($data);
}
